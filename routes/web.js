var express = require('express');
var router = express.Router();
var multer = require("multer");
var Sequelize = require("sequelize");

const Picture = require("../models/picture");

var storage = multer.diskStorage({
    destination: "public/images/",
    filename: function (req, file, cb) {

        // get type of picture
        let type = file.originalname.substr(file.originalname.lastIndexOf('.') + 1);

        cb(null, file.fieldname + '-' + Date.now() + "." + type)
    }
})

var upload = multer({
    storage: storage
});



var sequelize = new Sequelize(process.env.DB_DATABASE, process.env.DB_USERNAME, process.env.DB_PASSWORD, {
    host: process.env.DB_HOST,
    dialect: process.env.DB_CONNECTION,
});


/**
 * GET APP
 */
router.get('/', function(req, res, next) {

    var picture = Picture(sequelize, Sequelize);
    picture.findOne({
        where: {
            approved: 1
        },
        order: Sequelize.literal("rand()")
    }).then(pictureEntity => {
        res.render('index', {
            picture: pictureEntity,
            env: process.env
        });

        pictureEntity.visit_count += 1;
        pictureEntity.save();
    });

});

/**
 * GET APP
 */
router.get('/picture/share/:picturePID', function(req, res, next) {

    var picture = Picture(sequelize, Sequelize);
    picture.findOne({
        where: {
            pid: req.params.picturePID
        }
    }).then(pictureEntity => {
        res.render('index', {
            picture: pictureEntity,
            env: process.env
        });

        pictureEntity.visit_count += 1;
        pictureEntity.save();
    });

});

/**
 * GET APP
 */
router.get('/picture/download/:picturePID', function(req, res, next) {

    var picture = Picture(sequelize, Sequelize);
    picture.findOne({
        where: {
            pid: req.params.picturePID
        }
    }).then(pictureEntity => {

        var file = __dirname + "/../public/images/" + pictureEntity.safe_name;
        res.download(file);

        pictureEntity.download_count += 1;
        pictureEntity.save();
    });

});

/**
 * UPLOAD PICTURE
 */
router.post('/upload', upload.single("picture"), function(req, res, next) {

    // first perform validation
    var errors = new Array();

    if (typeof req.file == "undefined") {
        errors.push("picture");
    }
    if (req.body.title === "") {
        errors.push("title");
    }
    if (req.body.author === "") {
        errors.push("author");
    }
    if (!("checkbox_1" in req.body)) {
        errors.push("checkbox_1");
    }
    if (!("checkbox_2" in req.body)) {
        errors.push("checkbox_2");
    }

    if (errors.length > 0) {
        res.status(422);
        res.send({
            "errors": errors
        });
        return;
    }

    var picture = Picture(sequelize, Sequelize);

    picture.create({
        title: req.body.title,
        safe_name: req.file.filename,
        quote: req.body.quote,
        author: req.body.author,
        tags: req.body.tags,
        filesize: req.file.size,
        filesize_human_readable: (req.file.size / 1000000).toFixed(2),
        approved: 0,
        visit_count: 1,
        download_count: 0,
        is_on_instagram: 0,
        pid: generateUuid()
    }).then(function(x) {

        res.status(200);
        res.send({
            "result" : "Ok"
        });
    });

});

function generateUuid() {

    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();

}

module.exports = router;