'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
        "Pictures",
        "pid",
        Sequelize.STRING);
  },

  down: (queryInterface, Sequelize) => {
  }
};
