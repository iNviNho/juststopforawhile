'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Pictures', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      title: {
        type: Sequelize.STRING
      },
      safe_name: {
        type: Sequelize.TEXT
      },
      quote: {
        type: Sequelize.TEXT
      },
      filesize: {
        type: Sequelize.STRING
      },
      filesize_human_readable: {
        type: Sequelize.STRING
      },
      tags: {
        type: Sequelize.TEXT
      },
      author: {
        type: Sequelize.STRING
      },
      approved: {
        type: Sequelize.BOOLEAN
      },
      visit_count: {
        type: Sequelize.INTEGER
      },
      download_count: {
        type: Sequelize.INTEGER
      },
      is_on_instagram: {
        type: Sequelize.BOOLEAN
      },
      place: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Pictures');
  }
};