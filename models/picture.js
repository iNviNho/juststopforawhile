'use strict';
module.exports = (sequelize, DataTypes) => {
  const Picture = sequelize.define('Picture', {
    title: DataTypes.STRING,
    safe_name: DataTypes.TEXT,
    quote: DataTypes.TEXT,
    filesize: DataTypes.STRING,
    filesize_human_readable: DataTypes.STRING,
    tags: DataTypes.TEXT,
    author: DataTypes.STRING,
    approved: DataTypes.BOOLEAN,
    visit_count: DataTypes.INTEGER,
    download_count: DataTypes.INTEGER,
    is_on_instagram: DataTypes.BOOLEAN,
    place: DataTypes.STRING,
    pid: DataTypes.STRING
  }, {});
  Picture.associate = function(models) {
    // associations can be defined here
  };
  return Picture;
};