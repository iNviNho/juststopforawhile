$(function() {

    let footerAnimating = false;
    let footerOpened = false;

    $(".footer-icon").on("click", function() {

        if (footerAnimating) {
            return;
        }
        footerAnimating = true;

        let footerIcon = $(this);
        let footer = $(".footer");

        if (footerOpened) {

            footer.animate({
                bottom: "-100%"
            }, 850, function() {
                footerIcon.css("background-color", "#323f57");
                footerIcon.animate({
                    bottom: "20px"
                }, 150, function() {
                    footerOpened = false;
                    footerAnimating = false;
                });
            });


        } else {

            footerIcon.animate({
                bottom: "0px",
            }, 150, function() {
                footerIcon.css("background-color", "transparent");
                footer.animate({
                    bottom: "0"
                }, 850, function() {
                    footerOpened = true;
                    footerAnimating = false;
                });
            });

        }
    });


    let menuOpened = false;
    let menuAnimated = false;

    $(".menu").on("click", function() {

        if (menuAnimated) {
            return;
        }
        menuAnimated = true;
        let barsSpan = $(".bars");
        let closeSpan = $(".close-menu");

        let menuWrapper = $(".menu-wrapper");

        if (menuOpened) {

            closeSpan.hide();
            barsSpan.fadeIn();


            menuWrapper.removeClass("opened");
            setTimeout(function() {
                menuOpened = false;
                menuAnimated = false;
            }, 250);

        } else {

            barsSpan.hide();
            closeSpan.fadeIn();


            menuWrapper.addClass("opened");
            setTimeout(function() {
                menuOpened = true;
                menuAnimated = false;
            }, 250);

        }

    });

    var uploading = false;

    $("#uploadFormSubmit").on("click", function() {

        if (uploading) {
            return;
        }

        var form = $("#uploadForm");

        uploading = true;
        form.addClass("uploading");

        var data = new FormData(form[0]);

        form.find(".error").removeClass("error");

        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "/upload",
            data: data,
            processData: false,
            contentType: false,
            timeout: 600000,
            success: function (data) {
                form.trigger("reset");

                $("#uploadModal").modal("hide");
                $("#successModal").modal("show");

                form.removeClass("uploading");
                uploading = false;
            },
            error: function (e) {


                if (e.status === 422) {

                    var failedFields = e.responseJSON.errors;
                    failedFields.forEach(function(value, index) {
                        form.find("#form-"+value+"").addClass("error");
                    });

                }

                form.removeClass("uploading");
                uploading = false;

            }
        });

        return;
    });


    var loadingImage = new Image();
    loadingImage.onload = function() {
        $("#loading").fadeOut(500, function() {
           $(this).remove();
        });
    };
    loadingImage.src = $("#ĺoading_image").attr("src");


    let infoOpened = false;
    let infoAnimated = false;

    $(".info-icon").on("click", function() {

        if (infoAnimated) {
            return;
        }
        infoAnimated = true;

        let infoIcon = $(this);
        let info = $(".info");

        if (infoOpened) {

            info.removeClass("opened");
            infoIcon.css({
                left: 0
            });

            setTimeout(function() {
                infoOpened = false;
                infoAnimated = false;
            }, 250);

        } else {

            info.addClass("opened");
            infoIcon.css({
                left: info.outerWidth()
            });

            setTimeout(function() {
                infoOpened = true;
                infoAnimated = false;
            }, 250);

        }

    });


});